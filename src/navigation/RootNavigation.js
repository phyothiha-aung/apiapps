import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';

import GithubProfile from '../components/GithubProfile/GithubProfile';
import WeatherApp from '../components/WeatherApp/WeatherApp';
import Home from '../components/NewsReader/Home';
import Users from '../components/CRUD/Users';

const Drawer = createDrawerNavigator();

const RootNavigation = () => {
  return (
    <NavigationContainer>
      <Drawer.Navigator>
        <Drawer.Screen name="Github Profile" component={GithubProfile} />
        <Drawer.Screen name="Weather App" component={WeatherApp} />
        <Drawer.Screen name="News Reader" component={Home} />
        <Drawer.Screen name="Users" component={Users} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigation;
