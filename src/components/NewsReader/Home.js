import {FlatList, StyleSheet, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import axios from 'axios';

import NewsCard from './NewsCard';

const URL =
  'https://newsapi.org/v2/top-headlines?country=us&pageSize=20&apiKey=27820167d6b1430a990adbe821873a09';

const Home = () => {
  const [news, setNews] = useState([]);
  const [refreshing, setRefreshing] = useState(true);

  const fetchData = () => {
    setRefreshing(true);
    axios
      .get(URL)
      .then(res => {
        const data = res.data;
        setNews(data.articles);
        setRefreshing(false);
      })
      .catch(error => {
        console.log(error);
        setRefreshing(false);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <View style={styles.container}>
      <FlatList
        data={news}
        keyExtractor={item => item.title}
        renderItem={({item}) => <NewsCard item={item} />}
        refreshing={refreshing}
        onRefresh={fetchData}
      />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: '#000',
  },
});
