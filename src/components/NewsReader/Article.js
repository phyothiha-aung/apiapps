import {View, StyleSheet, Text, Image} from 'react-native';
import React from 'react';

const Article = ({route}) => {
  const news = route.params.news;
  return (
    <View>
      <Text style={styles.title}>{news.title}</Text>
      <Image
        style={styles.image}
        source={{
          uri: news.urlToImage,
        }}
      />
      <Text>Author: {news.author}</Text>
      <Text>{news.publishedAt}</Text>
      <Text style={styles.content}>{news.content}</Text>
    </View>
  );
};

export default Article;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5,
  },
  title: {
    padding: 5,
    fontSize: 20,
    color: '#000',
    fontWeight: 'bold',
  },
  image: {
    width: '100%',
    height: 300,
  },
});
