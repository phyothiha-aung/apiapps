import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import React from 'react';

const NewsCard = ({item}) => {
  return (
    <View style={styles.container}>
      <ImageBackground
        style={styles.bgImage}
        resizeMode="contain"
        resizeMethod="resize"
        source={{
          uri: item.urlToImage,
        }}>
        <Text style={styles.title}>{item.title}</Text>
      </ImageBackground>
      <View style={styles.content}>
        <Text numberOfLines={3} ellipsizeMode="tail" style={styles.description}>
          {item.description}
        </Text>
        <View style={styles.footer}>
          <Text style={styles.author} numberOfLines={1} ellipsizeMode="tail">
            {item.author}
          </Text>
          <Text
            style={styles.publishedAt}
            numberOfLines={1}
            ellipsizeMode="tail">
            {item.publishedAt}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default React.memo(NewsCard);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginTop: 10,
    borderWidth: 2,
    borderColor: 'white',
    padding: 5,
  },
  bgImage: {
    width: '100%',
    height: 300,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 3,
  },
  title: {
    fontSize: 30,
    color: 'white',
    fontWeight: 'bold',
  },
  content: {backgroundColor: 'white', padding: 10},
  description: {
    fontSize: 18,
    backgroundColor: 'white',
    color: 'black',
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    marginVertical: 10,
  },
  author: {
    fontSize: 15,
    width: '40%',
  },
  publishedAt: {
    fontSize: 15,
    width: '40%',
  },
});
