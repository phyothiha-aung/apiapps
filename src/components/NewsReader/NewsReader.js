import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Home from './Home';
import Article from './Article';

const Stack = createNativeStackNavigator();
const NewsReader = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen name="Article" component={Article} />
    </Stack.Navigator>
  );
};

export default NewsReader;
