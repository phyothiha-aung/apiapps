import {
  Button,
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import axios from 'axios';

const URL = 'https://jsonplaceholder.typicode.com/users';

const Users = () => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState('');
  const [users, setUsers] = useState([]);
  const [text, setText] = useState();

  const fetchUsers = () => {
    setLoading(true);
    axios
      .get(URL)
      .then(res => {
        const data = res.data;
        setUsers(data);
        setLoading(false);
      })
      .catch(error => {
        console.log(error);
        setError(error);
        setLoading(false);
      });
  };

  const addUser = name => {
    axios
      .post(URL, {name})
      .then(res => setUsers([...users, res.data]))
      .catch(error => console.log(error));
  };

  useEffect(() => {
    fetchUsers();
  }, []);
  return (
    <View style={styles.container}>
      {error && <Text>{error}</Text>}
      <View style={styles.input}>
        <TextInput
          placeholder="Enter Name"
          value={text}
          onChangeText={t => setText(t)}
        />
        <Button
          title="add"
          onPress={() => {
            addUser(text);
            setText('');
          }}
        />
      </View>
      <FlatList
        refreshing={loading}
        onRefresh={fetchUsers}
        data={users}
        keyExtractor={item => item.id}
        renderItem={({item}) => (
          <View style={styles.list}>
            <Text style={styles.name}>{item.name}</Text>
          </View>
        )}
      />
    </View>
  );
};

export default Users;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  list: {
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  name: {
    fontSize: 18,
    width: '80%',
    padding: 10,
    borderWidth: 1,
    borderColor: '#000',
  },
});
